package hw2;

import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    public static void main(String[] args) {

        Random random = new Random();

        System.out.println("All set. Get ready to rumble!");
        String [][] square = new String[6][6];
        fillMatrix(square);
        printMatrix(square);

        int randomTargetRow = random.nextInt(5) + 1;
        int randomTargetColumn = random.nextInt(5) + 1;

        System.out.println(randomTargetRow + "  "+ randomTargetColumn);

        int userRow;
        int userColumn;

        do {
            System.out.print("Please enter row number 1..5: ");
            userRow = scanUserNumber();
            System.out.print("Please enter column number 1..5: ");
            userColumn = scanUserNumber();
            boolean isUserRowValid = isUserNumberValid(userRow);
            boolean isUserColumnValid = isUserNumberValid(userColumn);
            if (isUserRowValid && isUserColumnValid) {
                square[userRow][userColumn] = "*";
                System.out.print("("+userRow+";"+userColumn+") - has been marked on the square. ");
            } else {
                System.out.println("Entered values are incorrect. Must be numbers 1..5. Try again.");
            }
            System.out.println("Current matrix is as below:");
            if (userRow == randomTargetRow && userColumn == randomTargetColumn){
                square[userRow][userColumn] = "X";
            }
            printMatrix(square);
        } while (userRow != randomTargetRow || userColumn != randomTargetColumn);

        System.out.println("You have won!");
        square[userRow][userColumn]="X";




    }

    public static int scanUserNumber(){
        Scanner scan = new Scanner(System.in);
        if (!scan.hasNextInt()) {
            return -1;
        }
        return scan.nextInt();
    }

    public static boolean isUserNumberValid(int userNumber) {
        return userNumber >= 1 && userNumber <= 5;
    }

    public static void printMatrix (String[][] matrix){
        for (String[] strings : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(" " + strings[j] + " |");
            }
            System.out.println();
        }
    }

    public static void fillMatrix (String[][] matrix) {
        for (int i=0; i < matrix.length; i++){
            for (int j=0; j < matrix.length; j++){
                if (i==0){
                    matrix [i][j] = Integer.toString(j);
                } else if (j==0){
                    matrix [i][j] = Integer.toString(i);
                } else {
                    matrix[i][j]="-";
                }
            }
        }
    }
}
