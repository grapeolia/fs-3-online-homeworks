package hw4;

import java.util.Arrays;

public class Human {
    private Family family;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;

    public Human() {

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        setYear(year);
    }

    public Human(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        setYear(year);
        setIq(iq);
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if(year>=1900 && year<=2022){
            this.year = year;
        } else {
            System.out.println("Year has incorrect value. Must be 1900...2022. Year 1900 has been set by default.");
            this.year = 1900;
        }
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if(iq>=0 && iq<=100){
            this.iq = iq;
        } else {
            System.out.println("IQ has incorrect value. Must be 0..100. IQ 0 has been set by default.");
            this.iq = 0;
        }
    }

    public Family getFamily(){
        return family;
    }
    public Human getMother() {
       if(this.getFamily()!=null) {
           for(Human human : this.getFamily().getChildren()){
               if(this == human){
                   return this.getFamily().getMother();
               }
           }
       }
       System.out.println("I'm not a child in this family.");
       return null;
    }

    public Human getFather() {
        if(this.getFamily()!=null) {
            for(Human human : this.getFamily().getChildren()){
                if(this == human){
                    return this.getFamily().getFather();
                }
            }
        }
        System.out.println("I'm not a child in this family.");
        return null;
    }
    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void greetPet(){
        if (this.getFamily() != null && this.getFamily().getPet() != null) {
            System.out.println("Hi, " + this.getFamily().getPet().getNickname() + "!");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname.");
        }
    }
    public void describePet(){
        if (this.getFamily() != null && this.getFamily().getPet() != null){
            String trickDescription = this.getFamily().getPet().getTrickLevel()>50
                    ? "very tricky"
                    : "barely tricky";
            System.out.println("I've got a "+this.getFamily().getPet().getSpecies()+
                    ". It's "+this.getFamily().getPet().getAge()+
                    ", it's "+trickDescription+".");
        } else {
            System.out.println("I don't have any pet.");
        }
    }

    @Override
    public String toString() {
        String motherRecord = (family != null && family.getMother() != null)
                ? (family.getMother().getName()+" "+ family.getMother().getSurname())
                : "no record";
        String fatherRecord = (family != null && family.getFather() != null)
                ? (family.getFather().getName() + " " + family.getFather().getSurname())
                : "no record";

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
