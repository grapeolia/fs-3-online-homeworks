package hw4;


import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family() {
        System.out.println("Parameters Mother & Father are must. Mother & Father are created by default. You can replace them by using set methods.");
        this.mother = new Human();
        this.father = new Human();
    }
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        String motherRecord = mother != null ? mother.toString() : "no name mentioned";
        String fatherRecord = father != null ? father.toString() : "no name mentioned";
        String petRecord = pet != null ? pet.toString() : "no record";

        return "Family{" +
                "mother=" + motherRecord +
                ", father=" + fatherRecord +
                ", children=" + Arrays.toString(children) +
                ", pet=" + petRecord +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family family)) return false;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, Arrays.hashCode(children), pet);
    }

    public void addChild(Human newChild){
        Human[] newChildren;
        if(this.getChildren() != null){
            newChildren = new Human[this.getChildren().length+1];
            System.arraycopy(children, 0, newChildren, 0, children.length);
        } else {
            newChildren = new Human[1];
        }
        newChildren[newChildren.length-1] = newChild;
        setChildren(newChildren);
    }

    public boolean deleteChild(int childIndex){
        if(this.getChildren() != null && this.getChildren().length>0) {
            if ((childIndex < 0) || (childIndex > this.getChildren().length-1)){
                return false;
            }
            Human[] newChildren = new Human[children.length-1];
            for (int i=0; i<newChildren.length; i++){
                if (i >= childIndex) {
                    newChildren[i] = children[i+1];
                } else {
                    newChildren[i] = children[i];
                }
            }
            this.children = newChildren;
            return true;
        }
        return false;
    }

    public int countFamily(){
        return children != null ? 2+children.length : 2;
    }
}
