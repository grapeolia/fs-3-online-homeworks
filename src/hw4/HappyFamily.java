package hw4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HappyFamily {
    public static void main(String[] args) {
        Human mother1 = new Human("Mother", "MotherLastName", 1979);
        Human father1 = new Human("Father", "FatherLastName", 1974);
        Human child1 = new Human("Child1", "ChildLastName", 2000);
        Human childToAdd = new Human("Child2","Child2Surname", 2019);
        String[][] schedule = setUpDailySchedule();
        System.out.println(Arrays.deepToString(father1.getSchedule()));
        Pet catBoria = new Pet("cat","Boria",2, 46, new String[]{"eat","sleep"});
        Pet dogRob = new Pet("dog", "Rob",3,67, new String[]{"eat","sleep","jump"});
        Family family1 = new Family();
        family1.setMother(mother1);
        family1.setFather(father1);
        System.out.println(family1);
        family1.setChildren(new Human[]{child1});
        family1.addChild(childToAdd);
        System.out.println("A child has been added. Children are "+ Arrays.toString(family1.getChildren()));
        family1.deleteChild(1);
        System.out.println("A child has been deleted. Children are "+Arrays.toString(family1.getChildren()));
        family1.deleteChild(1);
        System.out.println("A child with non-existing index has not been deleted. Children are "+Arrays.toString(family1.getChildren()));
        family1.deleteChild(0);
        System.out.println("A child has been deleted. Children are "+Arrays.toString(family1.getChildren()));

        family1.setPet(dogRob);
        System.out.println(family1);
        catBoria.respond();
        catBoria.eat();
        catBoria.foul();
        father1.setIq(-23);
        System.out.println(father1.getIq());

        child1.greetPet();
        father1.greetPet();
        father1.describePet();
        child1.describePet();


    }

    public static String[][] setUpDailySchedule(){
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[1][0] = "Tuesday";
        schedule[2][0] = "Wednesday";
        schedule[3][0] = "Thursday";
        schedule[4][0] = "Friday";
        schedule[5][0] = "Saturday";
        schedule[6][0] = "Sunday";

        schedule[0][1] = "Get Enough Sleep";
        schedule[1][1] = "Rise Early";
        schedule[2][1] = "Meditate";
        schedule[3][1] = "Workout";
        schedule[4][1] = "Eat A Good Breakfast";
        schedule[5][1] = "Take A Nap";
        schedule[6][1] = "Take Breaks To Re-energize";

        return schedule;
    }

}