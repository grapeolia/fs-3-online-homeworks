package hw3;

import java.util.Scanner;

public class Planning {
    public static void main(String[] args) {

        String[][] schedule = setUpDailySchedule();

        String userInput;
        boolean isUserInputADay;

//        String userEnteredDay = scan.next();
//        normalizeUserEnteredDay(userEnteredDay);
        do{
            System.out.print("Enter day of week: ");
            userInput = scanUserInput();
            userInput = normalizeUserInput(userInput);
            isUserInputADay = isUserInputADay(userInput);
            if (isUserInputADay) {
                printTaskForRequestedDay(schedule,userInput);
            } else if (!userInput.equals("Exit")) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        } while (!userInput.equals("Exit"));


    }

    public static void printTaskForRequestedDay(String[][] schedule, String day){
        switch (day) {
            case "Monday" -> System.out.println("Your tasks for Monday: " + schedule[0][1]);
            case "Tuesday" -> System.out.println("Your tasks for Tuesday: " + schedule[1][1]);
            case "Wednesday" -> System.out.println("Your tasks for Wednesday: " + schedule[2][1]);
            case "Thursday" -> System.out.println("Your tasks for Thursday: " + schedule[3][1]);
            case "Friday" -> System.out.println("Your tasks for Friday: " + schedule[4][1]);
            case "Saturday" -> System.out.println("Your tasks for Saturday: " + schedule[5][1]);
            case "Sunday" -> System.out.println("Your tasks for Sunday: " + schedule[6][1]);
            default -> {
            }
        }
    }
    public static boolean isUserInputADay (String userInput) {
        String[] days = new String[7];
        days[0] = "Monday";
        days[1] = "Tuesday";
        days[2] = "Wednesday";
        days[3] = "Thursday";
        days[4] = "Friday";
        days[5] = "Saturday";
        days[6] = "Sunday";
        for (String day : days) {
            if (day.equals(userInput)) {
                return true;
            }
        }
        return false;
    }

    public static String scanUserInput(){
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }
    public static String normalizeUserInput (String userInput) {
        userInput = userInput.replaceAll("\\s", "");
        return userInput.substring(0, 1).toUpperCase() + userInput.substring(1).toLowerCase();
    }

    public static String[][] setUpDailySchedule(){
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[1][0] = "Tuesday";
        schedule[2][0] = "Wednesday";
        schedule[3][0] = "Thursday";
        schedule[4][0] = "Friday";
        schedule[5][0] = "Saturday";
        schedule[6][0] = "Sunday";

        schedule[0][1] = "Get Enough Sleep";
        schedule[1][1] = "Rise Early";
        schedule[2][1] = "Meditate";
        schedule[3][1] = "Workout";
        schedule[4][1] = "Eat A Good Breakfast";
        schedule[5][1] = "Take A Nap";
        schedule[6][1] = "Take Breaks To Re-energize";

        return schedule;
    }

}
