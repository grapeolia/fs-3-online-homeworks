package hw1;

import java.lang.reflect.Array;
import java.util.*;

public class Numbers {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Random random = new Random();

        System.out.println("Let the game begin!");

        int randomNumber = random.nextInt(100);
        ArrayList<Integer> userNumbers = new ArrayList<>();

        System.out.print("Enter your name: ");
        String name = scan.next();

        System.out.println(name + randomNumber);

        int userNumber;
        int minRange = 0;
        int maxRange = 100;

        do {
            System.out.print("Enter your number "+minRange+".."+maxRange+": ");
            userNumber = scanUserNumber();
            boolean isUserNumberInRange = isUserNumberInRange(userNumber, minRange, maxRange);
            if (!isUserNumberInRange) {
                System.out.print("Value must be a number "+minRange+".."+maxRange+". ");
            } else if (userNumber < randomNumber) {
                System.out.println("Your number is too small. Please try again.");
                minRange = userNumber+1;
            } else if (userNumber > randomNumber) {
                System.out.println("Your number is big. Please try again.");
                maxRange = userNumber-1;
            }
            if (userNumber != -1) {
                userNumbers.add(userNumber);
                Collections.sort(userNumbers);
            }
        } while (userNumber != randomNumber);

        System.out.println("Congratulations, " + name + "!");
        System.out.println("Your numbers: "+userNumbers);

    }

    public static int scanUserNumber(){
        Scanner scan = new Scanner(System.in);
        if (!scan.hasNextInt()) {
            return -1;
        }
        return scan.nextInt();
    }

    public static boolean isUserNumberInRange(int userNumber, int minRange, int maxRange) {
        return userNumber >= minRange && userNumber <= maxRange;
    }

}
